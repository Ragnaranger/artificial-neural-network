# Artificial Neural Network

Almost generic ANN for Londrina's state university artificial intelligence assignment.



Objetivos:

- Importar dados mais rápido
- Implementar softmax
- Implementar funções de erro diferentes, como 'categorical cross entropy'

- Implementar otimizadores diferentes?




Configs que deram certo:
30, 25, 20, 10, lr=1, epochs=10_000, 97% depois de 2 fits
30, 25, 20, 15, 10, lr=1, epochs=10_000, 96.15% depois de 1 fit (melhor rate parece)
20, 20, 20, 10, lr=1, epochs=10_000, 95.8% depois de 1 fit
35, 30, 25, 20, 15, 10, lr=1, epochs=10_000, 94.39% depois de 1 fit (Parece que melhora com mais treino)
35, 30, 20, 20, 15, 10, lr=1, epochs=10_000, 94.39% depoisa de 1 fit (Parece que melhora com mais treino)