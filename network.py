import cupy as cp

from layer import Layer
from subject import Subject, Observer

from datetime import datetime
import _pickle
import os

# Vou tirar mais tarde
from utils import load_data, fix, check_result

class Network(Subject):
    def __init__(self, n_input, n_classes, name='no_name_network'):
        self.n_input = n_input
        self.n_classes = n_classes
        self.name = name
        self.layers = []
        self.n_layers = 0

        # Training hyperparameters
        self.learn_rate = 0
        self.n_epochs = 0
        self.decay = 0

        self.observers_on_1k_epochs = []

        # Useful info for observers
        self.mean_squared_error = float('inf')
        self.min_error = float('inf')

    
    def add_layer(self, n_neurons, activation_function='sigmoid'):
        if len(self.layers) == 0:
            layer = Layer(n_neurons, self.n_input, activation_function)
            self.layers.append(layer)
        else:
            layer = Layer(n_neurons, self.layers[-1].n_neurons, activation_function)
            self.layers.append(layer)

        self.n_layers += 1

    def last_layer(self, activation_function='sigmoid'):
        self.add_layer(self.n_classes, activation_function)

    # ----- Subject implementation -----
    def subscribe(self, observer: Observer, trigger:str):
        if trigger == 'on_1k_epochs':
            self.observers_on_1k_epochs.append(observer)

        else:
            raise Exception('Wrong trigger')

    def unsubscribe(self, observer: Observer, trigger:str):
        if trigger == 'on_1k_epochs':
            self.observers_on_1k_epochs.remove(observer)

        else:
            raise Exception('Wrong trigger')

    def notify(self, trigger):
        if trigger == 'on_1k_epochs':
            for observer in self.observers_on_1k_epochs:
                observer.update(self)
    # -----                        -----

    def view(self):
        print(f'Número de entradas: {self.n_input}')
        for layer in self.layers:
            print(f'Camada com: {layer.n_neurons} neurônios, função de ativação: {layer.activation_function_name}')


    # Feed foward
    def __predict(self, data):
        outputs = []
        outputs.append(data)

        for layer in self.layers:
            outputs.append(layer.predict(outputs[-1]))
    
        return outputs

    def predict(self, data):
        return self.__predict(data)[-1]

    def __validate(self, val_x, val_y):
        ans = self.predict(val_x)
        erro = val_y - ans
        return cp.mean(erro**2)

    def train(self, train_x, train_y, val_x, val_y, learn_rate=0.2, n_epochs=100000, decay=0.000001):
        # Saving hyperparameters
        self.learn_rate = learn_rate
        self.n_epochs = n_epochs
        self.decay = decay

        learn_rate = learn_rate/train_x.shape[0]
        best_epoch = 0
        epochs_w_no_change = 0
        # procurando_outro_minimo_mode = False


        print('Learn rate: ', learn_rate)
        print('Número de épocas: ', n_epochs)
        print('Decaimento: ', decay)
        print('Treinamento iniciado em: ', datetime.now())
        for epoch in range(0, n_epochs + 1):
            
            # Feed foward
            outputs = self.__predict(train_x)


            # Backpropagation
            layer = self.layers[-1]
            
            delta = 2*(outputs[-1] - train_y) * layer.d_activation_function(outputs[-1])

            layer.weights = layer.weights - learn_rate * cp.matmul(delta.T, outputs[-2])
            layer.bias = layer.bias - learn_rate * cp.sum(delta, axis=0)

            last_layer = layer

            # iter_layer = reversed(iter(self.layers))
            # next(iter_layer)
            for i in reversed(range(1, self.n_layers)):
                last_delta = delta
                layer = self.layers[i-1]

                delta = cp.matmul( last_delta, last_layer.weights) * layer.d_activation_function(outputs[i])
                layer.weights = layer.weights - learn_rate * cp.matmul(delta.T, outputs[i-1])
                layer.bias = layer.bias - learn_rate * cp.sum(delta, axis=0)

                last_layer = layer


            self.mean_squared_error = self.__validate(val_x, val_y)
            
            if self.mean_squared_error < self.min_error:
                best_epoch = epoch
                self.min_error = self.mean_squared_error
                # epochs_w_no_change = 0
                for layer in self.layers:
                    layer.best_weights = layer.weights
                    layer.best_bias = layer.bias

            # Learn rate decay
            learn_rate = learn_rate / (1 + decay * epoch)

            if epoch % 1000 == 0:
                self.train_error = self.__validate(train_x, train_y)
                self.notify('on_1k_epochs')

        print('Treinamento finalizado em: ', datetime.now())
        print('Melhor época: ', best_epoch)
        print('Menor erro: ', self.min_error)

        # Corrigindo layers
        for layer in self.layers:
            layer.weights = layer.best_weights
            layer.bias = layer.best_bias

    def save_network(self, export_folder='./'):
        file_name = os.getcwd()
        file_name = os.path.join(file_name, export_folder)
        file_name = os.path.join(file_name, self.name)
        file_name = file_name + '_'
        for layer in self.layers:
            file_name = file_name + f'{layer.n_neurons}_{layer.activation_function_name}_'
        file_name = file_name + f'neurons_{self.learn_rate}_learn_rate_{self.n_epochs}_epochs_{self.decay}_decay.pkl'

        with open(file_name, 'wb') as _file:
            _pickle.dump(self, _file, -1)
        
        print('Rede gravada em:\n', file_name)
    
    @classmethod
    def load_network(cls, file_name):
        with open(file_name, 'rb') as _file:
            return _pickle.load(_file)


if __name__ == "__main__":
    train_x, train_y, val_x, val_y, test_x, test_y = load_data('.\data\MNIST_normalized', 784)

    train_x = cp.array(train_x)
    train_y = cp.array(train_y)
    val_x = cp.array(val_x)
    val_y = cp.array(val_y)
    test_x = cp.array(test_x)
    test_y = cp.array(test_y)


    # model = Network.load_network('./novas_redes/Redezinha_128_10_neurons_0_learn_rate_0_epochs.pkl')
    model = Network(784, 10, name='teste_evento')
    # model.add_layer(128, activation_function='sigmoid')
    model.last_layer('sigmoid')

    model.view()
    model.train(train_x, train_y, val_x, val_y, learn_rate=0.8, n_epochs=15000, decay=0)
    
    model.save_network('./novas_redes')
    
    ans = model.predict(test_x)
    ans = fix(ans)
    print(ans[:3])
    check_result(ans, test_y)