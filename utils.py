import numpy as np
import cupy as cp
import pandas as pd
import matplotlib.pyplot as plt

from enum import Enum
from math import floor, ceil
from datetime import datetime
import os
import json
import codecs

def prepare_data(filename:str, n_features:int, delimiter:str, export_to='', seed=42):
    
     # Ler dataset do arquivo
    start = datetime.now()
    dataset = np.genfromtxt(filename, delimiter=delimiter)
    end = datetime.now()
    n_classes = len(dataset[0]) - n_features
    print('dados importados em: ', (end - start).seconds, ' segundos')
    # print(n_classes)
    

    # Fazer a primeira iteração de separação fora do for
    # para iniciar os dados de treino, validação e teste
    # e poder concatenar dentro do for

    # Separar classes
    _class = dataset[dataset[:, n_features] == 1]
    class_len = len(_class)
    
    # Embaralhar
    np.random.seed(seed)
    np.random.shuffle(_class)

    # Separar conjuntos de treinamento, validação e teste
    train_len = floor(class_len * 0.8)             # 80%
    val_len = ceil(ceil(class_len * 0.8) * 0.125)  # 10%
    test_len = class_len - (train_len + val_len)   # 10%
    
    train = _class[:ceil(train_len)]
    val = _class[ceil(train_len) : ceil(train_len) + ceil(val_len)]
    test = _class[ceil(train_len) + ceil(val_len):]

    for n_class in range(1, n_classes):

        # Separar classes
        _class = dataset[dataset[:, n_features + n_class] == 1]

        # Embaralhar
        np.random.seed(seed)
        np.random.shuffle(_class)

        # Separar conjuntos de treinamento, validação e teste
        class_len = len(_class)
        train_len = floor(class_len * 0.8)             # 80%
        val_len = ceil(ceil(class_len * 0.8) * 0.125)  # 10%
        test_len = class_len - (train_len + val_len)   # 10%
        
        train = np.append(train, _class[:train_len], axis=0)
        val = np.append(val, _class[train_len : train_len + val_len], axis=0)
        test = np.append(test, _class[train_len + val_len:], axis=0)


    # Embaralhar após separação
    np.random.seed(seed)
    np.random.shuffle(train)
    np.random.seed(seed)
    np.random.shuffle(val)
    np.random.seed(seed)
    np.random.shuffle(test)
    
    if export_to != '':
        np.savetxt(export_to+'_train.csv', train, delimiter=',')
        np.savetxt(export_to+'_val.csv', val, delimiter=',')
        np.savetxt(export_to+'_test.csv', test, delimiter=',')

    # Separar x e y
    train_x,    train_y   =  train[:, :n_features], train[:, n_features:]
    val_x,      val_y     =  val[:, :n_features],   val[:, n_features:]
    test_x,     test_y    =  test[:, :n_features],  test[:, n_features:]


    return train_x, train_y, val_x, val_y, test_x, test_y

def load_data(filename:str, n_features:int):
    train = np.genfromtxt(filename+'_train.csv', delimiter=',')
    val = np.genfromtxt(filename+'_val.csv', delimiter=',')
    test = np.genfromtxt(filename+'_test.csv', delimiter=',')

    # Separar x e y
    train_x,    train_y   =  train[:, :n_features], train[:, n_features:]
    val_x,      val_y     =  val[:, :n_features],   val[:, n_features:]
    test_x,     test_y    =  test[:, :n_features],  test[:, n_features:]

    return train_x, train_y, val_x, val_y, test_x, test_y

def fix(ans):
    ans = (ans == np.max(ans, axis=1)[:,None]).astype(int)
    return ans

def get_accuracy(nn_ans, labels):
    accuracy = 0

    for ans, label in zip(nn_ans, labels):
        if (ans == label).all():
            accuracy += 1


    accuracy = accuracy/nn_ans.shape[0]

    return accuracy

def check_result(nn_ans, _labels):
    labels = _labels.copy()
    n_classes = len(labels[0])

    true_positive = cp.zeros(n_classes)
    true_negative = cp.zeros(n_classes)
    false_positive = cp.zeros(n_classes)
    false_negative = cp.zeros(n_classes)

    for y, label in zip(nn_ans, labels):

        if (y == label).all():
            true_positive += label
            label[label == 1] = 2
            label[label == 0] = 1
            label[label == 2] = 0
            true_negative += label

        else:
            classe_correta = cp.argmax(label)
            predict_errado = cp.argmax(y)

            false_positive[predict_errado] += 1

            for i in range(n_classes):
                if i != classe_correta and i != predict_errado:
                    true_negative[i] += 1

            false_negative[classe_correta] += 1

    print('True  Positive: ', true_positive)
    print('True  Negative: ', true_negative)
    print('False Positive: ', false_positive)
    print('False Negative: ', false_negative)
    precision = true_positive / (true_positive + false_positive)
    recall = true_positive / (true_positive + false_negative)

    taxa_acerto = cp.sum(true_positive)/labels.shape[0]

    print('Precision: ', precision)
    print('Recall', recall)
    print('Taxa acerto ', taxa_acerto)

