from network import Network
from utils import *

from subject import Subject, Observer

import matplotlib.pyplot as plt
import cupy as cp

class Val_error_list(Observer):
    __list = []


    def update(self, subject: Subject):
        self.__list.append(subject.mean_squared_error)

    def get_list(self):
        return self.__list

class Train_error_list(Observer):
    __list = []


    def update(self, subject: Subject):
        self.__list.append(subject.train_error)
        # print(self.__list)

    def get_list(self):
        return self.__list

class Accuracy(Observer):
    def __init__(self, train_x, train_y, val_x, val_y):
        self.train_x = train_x
        self.train_y = train_y
        self.val_x = val_x
        self.val_y = val_y
        self.__train_accuracy = []
        self.__val_accuracy = []
    
    def update(self, subject: Subject):

        train_acc = get_accuracy(
            fix(subject.predict(self.train_x)),
            self.train_y
        )

        val_acc = get_accuracy(
            fix(subject.predict(self.val_x)), 
            self.val_y
        )


        self.__train_accuracy.append(train_acc)
        self.__val_accuracy.append(val_acc)
    
    def get_train(self):
        return self.__train_accuracy
    
    def get_val(self):
        return self.__val_accuracy

def plot_error_graph(val_error_list, train_error_list, step):
    y_val = np.array(val_error_list)
    y_train = np.array(train_error_list)

    xlim = step * len(val_error_list)
    x = np.arange(0, xlim, step)


    plt.xlim((0, xlim + step))
    plt.ylim((0, np.max(y_val) + 0.02))

    plt.plot(x, y_val, color='r')
    plt.plot(x, y_train, color='b')
    plt.legend(['val error', 'train error'], loc='upper right')
    plt.show()

def plot_acc_graph(val_acc_list, train_acc_list, step):
    
    y_val = np.array(val_acc_list)
    y_train = np.array(train_acc_list)
    xlim = step * len(val_acc_list)
    x = np.arange(0, xlim, step)


    plt.xlim((0, xlim + step))
    plt.ylim((0.05, 1.05))

    plt.plot(x, y_val, color='r')
    plt.plot(x, y_train, color='b')
    plt.legend(['val accuracy', 'train accuracy'], loc='lower right')
    plt.ylabel('Accuracy')
    plt.xlabel('1k epochs')
    
    plt.show()


if __name__ == "__main__":
    # train_x, train_y, val_x, val_y, test_x, test_y = prepare_data('.\\base_data\MNIST_normalized.csv', 784, ',', export_to='.\data\\novo_MNIST_normalized')
    train_x, train_y, val_x, val_y, test_x, test_y = load_data('.\data\\novo_MNIST_normalized', 784)
    # train_x, train_y, val_x, val_y, test_x, test_y = import_from_keras()

    train_x = cp.array(train_x)
    train_y = cp.array(train_y)
    val_x = cp.array(val_x)
    val_y = cp.array(val_y)
    test_x = cp.array(test_x)
    test_y = cp.array(test_y)

    # model = Network.load_network('./final/rede2.pkl')
    model = Network(784, 10, name='teste_antes_aula')
    model.add_layer(128, activation_function='sigmoid')
    model.add_layer(64, activation_function='sigmoid')
    # model.add_layer(25, activation_function='sigmoid')
    # model.add_layer(15, activation_function='sigmoid')
    model.last_layer(activation_function='sigmoid')

    val_error_list = Val_error_list()
    model.subscribe(val_error_list, 'on_1k_epochs')
    train_error_list = Train_error_list()
    model.subscribe(train_error_list, 'on_1k_epochs')
    acc_list = Accuracy(train_x, train_y, val_x, val_y)
    model.subscribe(acc_list, 'on_1k_epochs')

    model.view()
    model.train(train_x, train_y, val_x, val_y, learn_rate=0.5, n_epochs=10000, decay=0)
    

    model.save_network('./redes_trabalho')
    
    ans = model.predict(test_x)
    ans = fix(ans)
    # print(ans[:3])
    check_result(ans, test_y)

    plot_acc_graph(acc_list.get_val(), acc_list.get_train(), 1)
    plot_error_graph(val_error_list.get_list(), train_error_list.get_list(), 1)
