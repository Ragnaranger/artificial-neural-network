from __future__ import annotations
from abc import ABC, abstractmethod
# Uma classe abstrata para subjects
# Basicamente uma interface
class Subject(ABC):

    @abstractmethod    
    def subscribe(self, observer:Observer):
        pass


    @abstractmethod
    def unsubscribe(self, observer:Observer):
        pass


    @abstractmethod
    def notify(self):
        pass

class Observer(ABC):

    @abstractmethod
    def update(self, subject:Subject):
        pass